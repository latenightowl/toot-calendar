# Kalendář

**Table of Contents**

- [About](#about)
- [Usage](#usage)
- [Production](#production)
- [License](#license)


## About

`toot-calendar` (or Kalendář in Czech) is a Mastodon bot that periodically toots about calendar events.
Currently it's just namedays and public holidays, in the future it could be moon phases, sunraises and sunsets, and more.
Depends on how much time I'll have.

Official instance lives as <a rel="me" href="https://mastodon.arch-linux.cz/@kalendar">@kalendar@mastodon.arch-linux.cz</a>.


## Usage

Install with

```bash
python3 -m pip install git+https://codeberg.org/latenightowl/toot-calendar.git
```

or clone the repository and use Hatch, to develop locally.

Then you can run it from the terminal by invoking

```bash
toot-calendar --help
```

To use it, you will need an account on some Mastodon server.
The application will cache your environment variables, so you only need to use them the first time.

```bash
TOOT_SERVER=https://mastodon.example.org TOOT_EMAIL=me@example.com TOOT_PASSWORD=password toot-calendar --nothing
```

If everything works, you will see some logs and a note that the bot was able to log in.

Next time, you only have to run

```bash
toot-calendar --nothing
```

You can use cron to trigger the feature(s).
In a bright future with pink unicorns and beatiful flowers, this tool is a daemon that has its own schedule and even recovery plan in case of downtime.


## Production

See [USAGE.md](USAGE.md).


## License

`toot-calendar` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.

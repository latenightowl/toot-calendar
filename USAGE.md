# USAGE

## Nameday

On production, the bot is triggered by cron. For example:

```
1 0  * * * toot-calendar --action nameday
```

## Siren

In Czechia, we test sirens every first Wednesday of the month at noon.

```
0 12 * * * [ $(date +%u) -eq 3 -a $(date +%-d) -le 7 ] && toot-calendar --action siren
```

## Sunrises and sunsets

These two are interesting, because they are triggered at different time each day.

First, run

```
toot-calendar --prepare-maintenance-files
```

each day (e.g. at midnight).

This will write two timestamp files, `~/.config/toot-framework.toot-calendar/sunrise.timestamp` and `~/.config/toot-framework.toot-calendar/sunset.timestamp`, containing the date and time of this day's events.

Cron can be set up to trigger every minute* during some viable intervals; bash will check if the current time equals to the timestamp written into a file and will only execute if they match.

While you could do that directly in cron, it is easier to write it out in a bash script:

```bash
#!/bin/bash
# toot-calendar-sunset.sh

TIMESTAMP_FILE="$HOME/.config/toot-framework.toot-calendar/sunrise.timestamp"
if [ ! -f $TIMESTAMP_FILE ]; then
        printf "19700101T0000" > $TIMESTAMP_FILE
fi
TIMESTAMP=$(cat "$TIMESTAMP_FILE")
if [ ! $(date +%Y%m%dT%H%M) == "$TIMESTAMP" ]; then exit 0; fi

toot-calendar --action sunrise
```

> \*) Because this is Mastodon, it takes some time to distribute the message between the servers.
> Plus, the followers may not care about it being a bit off.
> If you want increase precision to minutes, pass `TOOT_CALENDAR_PRECISE=1` to the `--prepare-maintenance-files`.
> If you want to round the time, but as a lower ten minutes (e.g., if you want to wait for the right time), pass `TOOT_CALENDAR_LOWER=1`.

After that, you can trigger that from cron as well:

```
0,10,20,30,40,50 3-8   * * * bash toot-calendar-sunrise.sh
0,10,20,30,40,50 16-21 * * * bash toot-calendar-sunset.sh
```

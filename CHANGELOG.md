# CHANGELOG

**2.2.0 (2022-11-29)**

- Sun: Add Slovak mutation
- Nameday: Add Slovak mutation

**2.1.2 (2022-11-11)**

- Sun: Change the message a bit
- Sun: Get rid of the minute trigger code

**2.1.1 (2022-11-10)**

- `TOOT_CALENDAR_PRECISE`, `TOOT_CALENDAR_LOWER` environment variables

**2.1.0 (2022-11-10)**

- Refactor internal code
- Use `--prepare-maintenance-files` instead of minute triggers

**2.0.1 (2022-11-10)**

- Do not overwrite `toot()`
- Sun: Add minute trigger to disallow invocation at wrong time

**2.0.0 (2022-11-09)**

- Sunrise & Sunset feature
- Cleaned up internals
- Actions have to be invoked as `--action $ACTION`, breaking compatibility

**1.0.0 (2022-11-07)**

- New feature: Siréna
- Bump to major version, because I feel confident in stability of the code

**0.2.0 (2022-11-07)**

- Core was split into separate `toot-framework` package

**0.1.0 (2022-11-07)**

- Rewrite message handling, making the code cleaner

**0.0.0 (2022-11-06)**

- Initial release

# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
import datetime


def round_date(timestamp: datetime.datetime, lower: bool = False) -> datetime.datetime:
    minutes: int
    if lower:
        minutes = 10 * round(timestamp.minute // 10)
    else:
        minutes = 10 * round(timestamp.minute / 10)

    hours = timestamp.hour
    if minutes == 60:
        hours += 1
        minutes = 0
    return datetime.datetime(
        timestamp.year,
        timestamp.month,
        timestamp.day,
        hours,
        minutes,
        0,
        tzinfo=timestamp.tzinfo,
    )

# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
import datetime
from typing import Dict, List

import requests
from loguru import logger

from toot_framework import Message


class AbstractNameday:
    url: str = "https://svatky.adresa.info/json"

    lang: str = "???"
    days: List[str] = ["???"] * 7
    nameday_has: str = "???"

    def request(self, date: str = datetime.datetime.now()) -> dict:
        url = self.url + f"?lang={self.lang}&date={date.strftime('%d%m')}"
        logger.debug(f"Fetching data from {url}.")
        result = requests.get(url)
        if not result.content:
            raise RuntimeError(f"API returned no content for date {date}.")
        if result.status_code // 100 == 2:
            logger.debug(f"Got response {result.status_code}.")
            return result.json()
        raise RuntimeError(f"API returned {result.status_code} for date {date}.")

    def day_of_week(self, index: int) -> str:
        # Ensure the day of the week is czech; we can't rely on the system
        # being in the right locale. This app is not made to be i18ned.
        return self.days[index]

    def format(self, data: List[Dict[str, str]]) -> str:
        names = []
        holidays = []

        for entity in data:
            name = entity["name"]
            if " " in name:
                holidays.append(name)
            else:
                names.append(name)

        result: str = ""

        if names:
            result += f"{self.nameday_has} "
            if len(names) > 1:
                result += ", ".join(names[:-1]) + " a " + names[-1]
            else:
                result += names[-1]
            result += "."

        if names and holidays:
            result += " "

        if holidays:
            result += "Je "
            if len(holidays) > 1:
                result += ", ".join(holidays[:-1]) + " a " + holidays[-1]
            else:
                result += holidays[-1]
            result += "."

        return result


class CzechNameday(AbstractNameday):
    lang = "cs"
    days = ["pondělí", "úterý", "středa", "čtvrtek", "pátek", "sobota", "neděle"]
    nameday_has = "Svátek má"


class SlovakNameday(AbstractNameday):
    lang = "sk"
    days = ["pondělok", "utorok", "streda", "štvrtok", "piatok", "sobota", "neděľa"]
    nameday_has = "Meniny má"


class AbstractNamedayMessage(Message):
    def __init__(
        self,
        timestamp: datetime.datetime,
        nameday: type(AbstractNameday),
        tags: List[str],
    ):
        self.timestamp = timestamp
        self.nameday = nameday()

        text = self.nameday.format(self.nameday.request(timestamp))
        super().__init__(text, tags=tags)

    def __str__(self):
        day: str = self.nameday.day_of_week(self.timestamp.weekday())
        date: str = self.timestamp.strftime("%-d. %-m. %Y")

        fulldate: str = f"Je {day} {date}"
        # On Mondays, include week number
        if self.timestamp.weekday() == 0:
            weeknum = int(self.timestamp.strftime("%W"))
            fulldate += f", {weeknum}. {self.week}"
        fulldate += "."

        return f"{fulldate} {self.text}\n\n{self.tags}"


class CzechNamedayMessage(AbstractNamedayMessage):
    week = "týden roku"

    def __init__(self, timestamp: datetime.datetime):
        super().__init__(
            timestamp,
            CzechNameday,
            tags=["kalendář", "svátek", "czech"],
        )


class SlovakNamedayMessage(AbstractNamedayMessage):
    week = "týždeň roka"

    def __init__(self, timestamp: datetime.datetime):
        super().__init__(
            timestamp,
            SlovakNameday,
            tags=["kalendár", "meniny", "slovak"],
        )

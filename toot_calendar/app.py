# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
import argparse
import datetime
import os
from typing import Tuple

from loguru import logger

from toot_framework import App
from toot_calendar.dateutils import round_date
from toot_calendar.nameday import CzechNamedayMessage, SlovakNamedayMessage
from toot_calendar.siren import SirenMessage
from toot_calendar.sun import (
    Sun,
    CzechSunriseMessage,
    CzechSunsetMessage,
    SlovakSunriseMessage,
    SlovakSunsetMessage,
)


class CalendarApp(App):
    def __init__(self):
        super().__init__("toot-calendar", "cs")
        self.stdout: bool = False
        """Whether to print the message instead of posting it."""

    def do_nothing(self) -> None:
        logger.info("Doing nothing. Shutting down.")

    actions = {
        "cs-nameday": CzechNamedayMessage,
        "sk-nameday": SlovakNamedayMessage,
        "siren": SirenMessage,
        "cs-sunrise": CzechSunriseMessage,
        "cs-sunset": CzechSunsetMessage,
        "sk-sunrise": SlovakSunriseMessage,
        "sk-sunset": SlovakSunsetMessage,
    }

    def prepare_maintenance_files(self) -> None:
        """Prepare maintenance files."""
        sun = Sun()
        sunrise, sunset = sun.compute(datetime.date.today())

        precise: bool = True if os.getenv("TOOT_CALENDAR_PRECISE", "") else False
        lower: bool = True if os.getenv("TOOT_CALENDAR_LOWER", "") else False
        if not precise:
            sunrise = round_date(sunrise, lower)
            sunset = round_date(sunset, lower)

        sunrise_path = (self._config_directory / "sunrise.timestamp").absolute()
        sunset_path = (self._config_directory / "sunset.timestamp").absolute()

        with sunrise_path.open("w") as handle:
            timestamp: str = sunrise.strftime("%Y%m%dT%H%M")
            handle.write(timestamp)
            logger.debug(f"Wrote '{timestamp}' to {sunset_path}.")
        with sunset_path.open("w") as handle:
            timestamp: str = sunset.strftime("%Y%m%dT%H%M")
            handle.write(timestamp)
            logger.debug(f"Wrote '{timestamp}' to {sunset_path}.")
        logger.info(f"Maintenance files prepared (precise={precise}, lower={lower}).")

    def do(self, action: str) -> None:
        Message = self.actions[action]

        now = datetime.datetime.now()
        message = Message(now)

        if self.stdout:
            print(message)
        else:
            toot: dict = self.toot(message)
            logger.info(f"Toot sent as {toot['url']}.")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--nothing",
        action="store_true",
        help="try to login and exit",
    )
    parser.add_argument(
        "--print",
        action="store_true",
        default=False,
        help="do not send; print the toot to stdout and exit",
    )
    parser.add_argument(
        "--prepare-maintenance-files",
        action="store_true",
        help="prepare maintenance files and exit",
    )
    parser.add_argument(
        "--action",
        choices=list(CalendarApp.actions.keys()),
    )
    args = parser.parse_args()

    app = CalendarApp()

    if args.nothing:
        app.do_nothing()
        return

    if args.prepare_maintenance_files:
        app.prepare_maintenance_files()
        return

    if args.print:
        app.stdout = True

    if args.action:
        app.do(action=args.action)
        return

    parser.print_help()

# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
import datetime
import subprocess
from typing import List, Literal, Tuple

import suntime
from loguru import logger

from toot_framework import Message

# fmt: off
HOURS = {
    "cs": [
        "", "jednu", "dvě", "tři", "čtyři", "pět", "šest", "sedm", "osm",
        "devět", "deset", "jedenáct", "dvanáct", "třináct", "čtrnáct",
        "patnáct", "šestnáct", "sedmnáct", "osmnáct", "devatenáct",
        "dvacet", "dvacet jedna", "dvacet dva", "dvacet tři",
    ],
    "sk": [
        "", "jednu", "dve", "tri", "štyri", "päť", "šesť", "sedem", "osem",
        "deväť", "desať", "jedenásť", "dvanásť", "trinásť", "štrnásť",
        "pätnásť", "šestnásť", "sedemnásť", "osemnásť", "devätnásť",
        "dvadsať", "dvadsaťjeden", "dvadsaťdva", "dvadsaťtri",
    ]
}
QUARTERS = {
    "cs": ["čtvrt", "půl", "tři čtvrtě"],
    "sk": ["štvrť", "pol", "trištvrte"],
}
# fmt: on


def format_timedelta(delta: datetime.timedelta, lang: str) -> str:
    hours = HOURS[lang]
    # This function breaks at its edges.
    # Times under one hour will break.
    # Times over twenty three hours will break.

    # This is just so the formatter does not explode it

    # timedelta only has seconds
    h: int = delta.seconds // 3600
    m: int = delta.seconds % 3600 // 60

    # 00..08 none
    # 09..23 quarter
    # 24..38 half
    # 39..53 three quarters
    # 54..59 none

    # overflow
    if m >= 54:
        h += 1
        m = 0

    tokens: List[str] = []

    # hours
    if h == 0 and m >= 54:
        tokens.append(hours[1])
    if h >= 1:
        tokens.append(hours[h])

    # joiner
    if h > 0 and m >= 9 and m <= 53:
        tokens.append("a")

    # minutes and units
    if m <= 8 or m >= 54:
        # special cases
        if h == 1:
            tokens.append("hodinu")
        if h >= 2 and h <= 4:
            tokens.append("hodiny")
        if h >= 5:
            tokens.append("hodin")
    if m >= 9 and m <= 23:
        tokens.append(QUARTERS[lang][0])
        tokens.append("hodiny")
    if m >= 24 and m <= 38:
        tokens.append(QUARTERS[lang][1])
        tokens.append("hodiny")
    if m >= 39 and m <= 53:
        tokens.append(QUARTERS[lang][2])
        tokens.append("hodiny")

    return " ".join(tokens)


class Sun:
    latitude: float = 49.19
    longitude: float = 16.60

    def get_system_offset(self) -> str:
        """Get system timezone. Only works on Linux."""
        output = subprocess.run(["timedatectl"], capture_output=True)
        stdout: List[str] = output.stdout.decode("utf-8").split("\n")
        tz_line: str = [line for line in stdout if "Time zone" in line][0].strip()
        # > Time zone: Europe/Prague (CET, +0100)
        offset: str = tz_line.split(" ")[-1][:-1]
        return offset

    def get_tz(self, offset: str) -> datetime.timezone:
        sign: Literal["+", "-"] = offset[0]
        hours = int(offset[1:3])
        minutes = int(offset[3:5])

        delta = datetime.timedelta(hours=hours, minutes=minutes)
        if sign == "-":
            delta = -delta
        return datetime.timezone(offset=delta)

    def to_local(self, timestamp: datetime.datetime) -> datetime.datetime:
        """Convert UTC timestamp to local timestamp."""
        offset = self.get_system_offset()
        tz = self.get_tz(offset)
        return timestamp.astimezone(tz)

    def compute(
        self, timestamp: datetime.datetime
    ) -> Tuple[datetime.datetime, datetime.datetime]:
        """Compute sunrise and sunset in UTC."""
        sun = suntime.Sun(self.latitude, self.longitude)
        sunrise = sun.get_local_sunrise_time(timestamp)
        sunset = sun.get_local_sunset_time(timestamp)

        logger.debug(f"{timestamp}: sunrise {sunrise}, sunset {sunset}")
        return sunrise, sunset

    def _test_for_sunrise(self, timestamp: datetime.datetime) -> bool:
        """Return True if the sun is raising at given time."""
        sunrise, _ = self.compute(timestamp)
        return sunrise.hour == timestamp.hour and sunrise.minute == timestamp.minute

    def _test_for_sunset(self, timestamp: datetime.datetime) -> bool:
        """Return True if the sun is setting at given time."""
        _, sunset = self.compute(timestamp)
        return sunset.hour == timestamp.hour and sunset.minute == timestamp.minute

    def format_sunrise(self, timestamp: datetime.datetime, lang: str) -> str:
        today = self.compute(timestamp)
        sunrise = today[0].strftime("%-H:%M")
        delta = format_timedelta(today[1] - today[0], lang)

        if lang == "cs":
            return f"Slunce vyšlo v {sunrise}, den potrvá {delta}."
        if lang == "sk":
            return f"Slnko vyšlo v {sunrise}, deň potrvá {delta}."
        raise RuntimeError(f"Unknown language '{lang}'.")

    def format_sunset(self, timestamp: datetime.datetime, lang: str) -> str:
        today = self.compute(timestamp)
        tomorrow = self.compute(timestamp + datetime.timedelta(days=1))
        sunset = today[1].strftime("%-H:%M")
        delta = format_timedelta(tomorrow[0] - today[1], lang)

        if lang == "cs":
            return f"Slunce zašlo v {sunset}, noc potrvá {delta}."
        if lang == "sk":
            return f"Slnko zašlo v {sunset}, noc potrvá {delta}."
        raise RuntimeError(f"Unknown language '{lang}'.")


class SunMessage(Message):
    def __init__(
        self,
        timestamp: datetime.datetime,
        tags: List[str],
        sunrise: bool,
        sunset: bool,
        lang: str,
    ):
        self.timestamp = timestamp
        self.sun = Sun()

        text: str
        if sunrise:
            text = self.sun.format_sunrise(timestamp, lang)
        elif sunset:
            text = self.sun.format_sunset(timestamp, lang)
        else:
            raise RuntimeError("Cannot do sunrise and sunset at the same time.")

        super().__init__(text, tags=tags)


class CzechSunriseMessage(SunMessage):
    def __init__(self, timestamp: datetime.datetime):
        super().__init__(
            timestamp,
            ["kalendář", "slunce", "VýchodSlunce", "czech"],
            sunrise=True,
            sunset=False,
            lang="cs",
        )


class CzechSunsetMessage(SunMessage):
    def __init__(self, timestamp: datetime.datetime):
        super().__init__(
            timestamp,
            ["kalendář", "slunce", "ZápadSlunce", "czech"],
            sunrise=False,
            sunset=True,
            lang="cs",
        )


class SlovakSunriseMessage(SunMessage):
    def __init__(self, timestamp: datetime.datetime):
        super().__init__(
            timestamp,
            ["kalendár", "slnko", "VýchodSlnka", "slovak"],
            sunrise=True,
            sunset=False,
            lang="sk",
        )


class SlovakSunsetMessage(SunMessage):
    def __init__(self, timestamp: datetime.datetime):
        super().__init__(
            timestamp,
            ["kalendár", "slnko", "ZápadSlnka", "slovak"],
            sunrise=False,
            sunset=True,
            lang="sk",
        )

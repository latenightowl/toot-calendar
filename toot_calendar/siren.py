# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
import datetime
from typing import Dict, List

import requests
from loguru import logger

from toot_framework import Message


class SirenMessage(Message):
    def __init__(self, _: datetime.datetime):
        text: str = "T" + ("u" * 191)
        super().__init__(text, tags=["siréna", "středa", "ZkouškaSirén", "czech"])

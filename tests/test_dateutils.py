# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
import datetime
from unittest import mock

from toot_calendar.dateutils import round_date

import pytest


@pytest.mark.parametrize(
    "h0,m0,lower,h1,m1",
    [
        (14, 20, False, 14, 20),
        (14, 25, False, 14, 20),
        (14, 26, False, 14, 30),
        (14, 26, True, 14, 20),
        (14, 56, False, 15, 0),
        (14, 56, True, 14, 50),
    ],
)
def test_round_date(h0: int, m0: int, lower: bool, h1: int, m1: int):
    timestamp = datetime.datetime(2022, 8, 16, h0, m0)
    result = round_date(timestamp, lower)

    assert result.hour == h1
    assert result.minute == m1

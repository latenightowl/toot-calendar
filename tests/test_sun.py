# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
import datetime
import subprocess
from unittest import mock

from toot_calendar.sun import (
    format_timedelta,
    Sun,
    CzechSunriseMessage,
    CzechSunsetMessage,
    SlovakSunriseMessage,
    SlovakSunsetMessage,
)

import pytest


@pytest.mark.parametrize(
    "expected,delta",
    [
        ("čtvrt hodiny", datetime.timedelta(hours=0, minutes=9)),
        ("půl hodiny", datetime.timedelta(hours=0, minutes=24)),
        ("tři čtvrtě hodiny", datetime.timedelta(hours=0, minutes=39)),
        ("jednu hodinu", datetime.timedelta(hours=0, minutes=54)),
        ("jednu a čtvrt hodiny", datetime.timedelta(hours=1, minutes=9)),
        ("jednu a půl hodiny", datetime.timedelta(hours=1, minutes=24)),
        ("jednu a tři čtvrtě hodiny", datetime.timedelta(hours=1, minutes=39)),
        ("dvě hodiny", datetime.timedelta(hours=1, minutes=54)),
    ],
)
def test_format_timedelta__lower(expected: str, delta: datetime.timedelta):
    assert expected == format_timedelta(delta, "cs")


@pytest.mark.parametrize(
    "expected,delta",
    [
        ("čtvrt hodiny", datetime.timedelta(hours=0, minutes=23)),
        ("půl hodiny", datetime.timedelta(hours=0, minutes=38)),
        ("tři čtvrtě hodiny", datetime.timedelta(hours=0, minutes=53)),
        ("jednu hodinu", datetime.timedelta(hours=1, minutes=8)),
        ("jednu a čtvrt hodiny", datetime.timedelta(hours=1, minutes=23)),
        ("jednu a půl hodiny", datetime.timedelta(hours=1, minutes=38)),
        ("jednu a tři čtvrtě hodiny", datetime.timedelta(hours=1, minutes=53)),
        ("jedenáct a půl hodiny", datetime.timedelta(hours=11, minutes=25)),
        ("čtrnáct hodin", datetime.timedelta(hours=14, minutes=0)),
        ("devatenáct a tři čtvrtě hodiny", datetime.timedelta(hours=19, minutes=50)),
    ],
)
def test_format_timedelta__upper(expected: str, delta: datetime.timedelta):
    assert expected == format_timedelta(delta, "cs")


@pytest.mark.parametrize(
    "expected,delta",
    [
        ("jednu a štvrť hodiny", datetime.timedelta(hours=1, minutes=23)),
        ("jednu a pol hodiny", datetime.timedelta(hours=1, minutes=38)),
        ("jednu a trištvrte hodiny", datetime.timedelta(hours=1, minutes=39)),
        ("štrnásť hodin", datetime.timedelta(hours=14, minutes=0)),
        ("devätnásť a trištvrte hodiny", datetime.timedelta(hours=19, minutes=50)),
    ],
)
def test_format_timedelta__sk(expected: str, delta: datetime.timedelta):
    assert expected == format_timedelta(delta, "sk")


@mock.patch("subprocess.run")
def test_Sun__offset(m__subprocess_run):
    completed = mock.Mock(spec=subprocess.CompletedProcess)
    completed.stdout = (
        b"                   Local time: Wed 2022-11-09 01:26:45 CET\n"
        b"           Universal time: Wed 2022-11-09 00:26:45 UTC\n"
        b"                 RTC time: Wed 2022-11-09 00:26:45    \n"
        b"                Time zone: Europe/Prague (CET, +0300) \n"
        b"System clock synchronized: yes                        \n"
        b"              NTP service: active                     \n"
        b"          RTC in local TZ: no                         \n"
    )
    m__subprocess_run.return_value = completed

    assert "+0300" == Sun().get_system_offset()


@pytest.mark.parametrize(
    "offset,expected",
    [
        ("+0300", datetime.timedelta(hours=3)),
        ("+0000", datetime.timedelta(hours=0)),
        ("-0200", datetime.timedelta(hours=-2)),
        ("-0230", datetime.timedelta(hours=-3, minutes=30)),
    ],
)
def test_Sun__get_tz(offset: str, expected: datetime.timedelta):
    assert datetime.timezone(offset=expected) == Sun().get_tz(offset)


@mock.patch("toot_calendar.sun.Sun.get_system_offset")
@pytest.mark.parametrize(
    "hour,offset,expected",
    [
        (14, "+0000", 14),
        (14, "+0400", 18),
        (22, "+0400", 2),
        (14, "-0300", 11),
    ],
)
def test_Sun__to_local(m__Sun_get_system_offset, hour: int, offset: str, expected: int):
    m__Sun_get_system_offset.return_value = offset

    timestamp = datetime.datetime(2022, 9, 17, hour, 0, 0, tzinfo=datetime.timezone.utc)
    result = Sun().to_local(timestamp)

    assert result.hour == expected


@pytest.mark.parametrize(
    "hour,minute,expected",
    [
        (6, 59, False),
        (7, 0, True),
        (7, 1, False),
    ],
)
def test_Sun__test_for_sunrise(hour: int, minute: int, expected: bool):
    timestamp = datetime.datetime(2022, 11, 13, hour, minute)
    assert expected == Sun()._test_for_sunrise(timestamp)


@pytest.mark.parametrize(
    "hour,minute,expected",
    [
        (16, 14, False),
        (16, 15, True),
        (16, 16, False),
    ],
)
def test_Sun__test_for_sunset(hour: int, minute: int, expected: bool):
    timestamp = datetime.datetime(2022, 11, 13, hour, minute)
    assert expected == Sun()._test_for_sunset(timestamp)


@pytest.mark.parametrize(
    "time,delta,timestamp",
    [
        ("6:54", "devět a půl hodiny", datetime.date(2022, 11, 9)),
        ("6:23", "jedenáct a půl hodiny", datetime.date(2023, 3, 8)),
        ("4:55", "šestnáct hodin", datetime.date(2023, 7, 4)),
    ],
)
def test_Sun__format_sunrise(time: str, delta: str, timestamp: datetime.datetime):
    expected = f"Slunce vyšlo v {time}, den potrvá {delta}."
    assert expected == Sun().format_sunrise(timestamp, "cs")


@pytest.mark.parametrize(
    "time,delta,timestamp",
    [
        ("16:20", "čtrnáct a půl hodiny", datetime.date(2022, 11, 9)),
        ("17:47", "dvanáct a půl hodiny", datetime.date(2023, 3, 8)),
        ("21:01", "osm hodin", datetime.date(2023, 7, 4)),
    ],
)
def test_Sun__format_sunset(time: str, delta: str, timestamp: datetime.datetime):
    expected = f"Slunce zašlo v {time}, noc potrvá {delta}."
    assert expected == Sun().format_sunset(timestamp, "cs")


def test_CzechSunriseMessage():
    timestamp = datetime.date(2022, 12, 23)
    message = CzechSunriseMessage(timestamp)

    expected = (
        "Slunce vyšlo v 7:47, den potrvá osm a čtvrt hodiny."
        "\n\n#kalendář #slunce #VýchodSlunce #czech"
    )
    assert expected == str(message)


def test_CzechSunsetMessage():
    timestamp = datetime.date(2022, 12, 23)
    message = CzechSunsetMessage(timestamp)

    expected = (
        "Slunce zašlo v 15:58, noc potrvá patnáct a tři "
        "čtvrtě hodiny.\n\n#kalendář #slunce #ZápadSlunce #czech"
    )
    assert expected == str(message)


def test_SlovakSunriseMessage():
    timestamp = datetime.date(2022, 12, 23)
    message = SlovakSunriseMessage(timestamp)

    expected = (
        "Slnko vyšlo v 7:47, deň potrvá osem a štvrť hodiny."
        "\n\n#kalendár #slnko #VýchodSlnka #slovak"
    )
    assert expected == str(message)


def test_SlovakSunsetMessage():
    timestamp = datetime.date(2022, 12, 23)
    message = SlovakSunsetMessage(timestamp)

    expected = (
        "Slnko zašlo v 15:58, noc potrvá pätnásť a trištvrte "
        "hodiny.\n\n#kalendár #slnko #ZápadSlnka #slovak"
    )
    assert expected == str(message)

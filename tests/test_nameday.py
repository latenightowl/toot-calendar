# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
import datetime
from unittest import mock

from toot_calendar.nameday import (
    CzechNameday,
    CzechNamedayMessage,
    SlovakNameday,
    SlovakNamedayMessage,
)

import pytest


def test_nothing():
    data = []
    result = SlovakNameday().format(data)
    assert result == ""


def test_one_name():
    data = [{"date": "1911", "name": "Alžběta"}]
    result = CzechNameday().format(data)
    assert result == "Svátek má Alžběta."


def test_two_names():
    data = [
        {"date": "1611", "name": "Otmar"},
        {"date": "1611", "name": "Mahulena"},
    ]
    result = CzechNameday().format(data)
    assert result == "Svátek má Otmar a Mahulena."


def test_one_holiday():
    data = [{"date": "2512", "name": "1. svátek vánoční"}]
    result = CzechNameday().format(data)
    assert result == "Je 1. svátek vánoční."


def test_two_holidays():
    data = [
        {"date": "0101", "name": "Den obnovy samostatného českého státu"},
        {"date": "0101", "name": "Nový rok"},
    ]
    result = CzechNameday().format(data)
    assert result == "Je Den obnovy samostatného českého státu a Nový rok."


def test_one_name_one_holiday():
    data = [
        {"date": "2612", "name": "Štěpán"},
        {"date": "2612", "name": "2. svátek vánoční"},
    ]
    result = CzechNameday().format(data)
    assert result == "Svátek má Štěpán. Je 2. svátek vánoční."


def test_two_names_and_holidays():
    data = [
        {"date": "2412", "name": "Eva"},
        {"date": "2412", "name": "Adam"},
        {"date": "2412", "name": "Štědrý den"},
    ]
    result = CzechNameday().format(data)
    assert result == "Svátek má Eva a Adam. Je Štědrý den."


def test_day_of_week():
    assert "pondělí" == CzechNameday().day_of_week(0)
    assert "pátek" == CzechNameday().day_of_week(4)
    assert "neděle" == CzechNameday().day_of_week(6)


@mock.patch("requests.get")
def test_request__200(m__requests_get):
    m__requests_get.return_value.status_code = 200

    CzechNameday().request(datetime.datetime(2022, 11, 9, 0, 1, 0))

    m__requests_get.assert_called_once_with(
        "https://svatky.adresa.info/json?lang=cs&date=0911"
    )


@mock.patch("requests.get")
def test_request__203(m__requests_get):
    m__requests_get.return_value.status_code = 203

    CzechNameday().request(datetime.datetime(2022, 11, 9, 0, 1, 0))

    m__requests_get.assert_called_once_with(
        "https://svatky.adresa.info/json?lang=cs&date=0911"
    )


@mock.patch("requests.get")
def test_request__200_no_content(m__requests_get):
    m__requests_get.return_value.status_code = 200
    m__requests_get.return_value.content = b""

    with pytest.raises(RuntimeError) as excinfo:
        CzechNameday().request(datetime.datetime(2022, 11, 9, 0, 1, 0))

    m__requests_get.assert_called_once_with(
        "https://svatky.adresa.info/json?lang=cs&date=0911"
    )
    assert str(excinfo.value) == "API returned no content for date 2022-11-09 00:01:00."


@mock.patch("requests.get")
def test_request__200_no_content(m__requests_get):
    m__requests_get.return_value.status_code = 404

    with pytest.raises(RuntimeError) as excinfo:
        CzechNameday().request(datetime.datetime(2022, 11, 9, 0, 1, 0))

    m__requests_get.assert_called_once_with(
        "https://svatky.adresa.info/json?lang=cs&date=0911"
    )
    assert str(excinfo.value) == "API returned 404 for date 2022-11-09 00:01:00."


@mock.patch("toot_calendar.nameday.CzechNameday.request")
def test_CzechNamedayMessage__20221107(m__nameday_request):
    m__nameday_request.return_value = [
        {"date": "0711", "name": "Saskie"},
    ]

    tzinfo = datetime.timezone(offset=datetime.timedelta(hours=1), name="Europe/Prague")
    timestamp = datetime.datetime(2022, 11, 7, 0, 1, 0, tzinfo=tzinfo)
    message = CzechNamedayMessage(timestamp)

    message.nameday.request = mock.MagicMock()
    message.nameday.request.return_value = {}

    expected = (
        "Je pondělí 7. 11. 2022, 45. týden roku. "
        "Svátek má Saskie.\n\n#kalendář #svátek #czech"
    )
    assert expected == str(message)


@mock.patch("toot_calendar.nameday.CzechNameday.request")
def test_CzechNamedayMessage__20221212(m__nameday_request):
    m__nameday_request.return_value = [
        {"date": "1212", "name": "Simona"},
    ]

    tzinfo = datetime.timezone(offset=datetime.timedelta(hours=1), name="Europe/Prague")
    timestamp = datetime.datetime(2022, 12, 12, 0, 1, 0, tzinfo=tzinfo)
    message = CzechNamedayMessage(timestamp)

    message.nameday.request = mock.MagicMock()
    message.nameday.request.return_value = {}

    expected = (
        "Je pondělí 12. 12. 2022, 50. týden roku. Svátek má Simona."
        "\n\n#kalendář #svátek #czech"
    )
    assert expected == str(message)


@mock.patch("toot_calendar.nameday.CzechNameday.request")
def test_CzechNamedayMessage__20221224(m__nameday_request):
    m__nameday_request.return_value = [
        {"date": "2412", "name": "Eva"},
        {"date": "2412", "name": "Adam"},
        {"date": "2412", "name": "Štědrý den"},
    ]

    tzinfo = datetime.timezone(offset=datetime.timedelta(hours=1), name="Europe/Prague")
    timestamp = datetime.datetime(2022, 12, 24, 0, 1, 0, tzinfo=tzinfo)
    message = CzechNamedayMessage(timestamp)

    message.nameday.request = mock.MagicMock()
    message.nameday.request.return_value = {}

    expected = (
        "Je sobota 24. 12. 2022. Svátek má Eva a Adam. Je Štědrý den."
        "\n\n#kalendář #svátek #czech"
    )
    assert expected == str(message)


@mock.patch("toot_calendar.nameday.CzechNameday.request")
def test_CzechNamedayMessage__20230102(m__nameday_request):
    m__nameday_request.return_value = [
        {"date": "0102", "name": "Hynek"},
    ]

    tzinfo = datetime.timezone(offset=datetime.timedelta(hours=1), name="Europe/Prague")
    timestamp = datetime.datetime(2023, 1, 2, 0, 1, 0, tzinfo=tzinfo)
    message = CzechNamedayMessage(timestamp)

    message.nameday.request = mock.MagicMock()
    message.nameday.request.return_value = {}

    expected = (
        "Je pondělí 2. 1. 2023, 1. týden roku. Svátek má Hynek."
        "\n\n#kalendář #svátek #czech"
    )
    assert expected == str(message)


@mock.patch("toot_calendar.nameday.SlovakNameday.request")
def test_SlovakNamedayMessage__20221107(m__nameday_request):
    m__nameday_request.return_value = [
        {"date": "0711", "name": "René"},
    ]

    tzinfo = datetime.timezone(offset=datetime.timedelta(hours=1), name="Europe/Prague")
    timestamp = datetime.datetime(2022, 11, 7, 0, 1, 0, tzinfo=tzinfo)
    message = SlovakNamedayMessage(timestamp)

    message.nameday.request = mock.MagicMock()
    message.nameday.request.return_value = {}

    expected = (
        "Je pondělok 7. 11. 2022, 45. týždeň roka. "
        "Meniny má René.\n\n#kalendár #meniny #slovak"
    )
    assert expected == str(message)


@mock.patch("toot_calendar.nameday.SlovakNameday.request")
def test_SlovakNamedayMessage__20221212(m__nameday_request):
    m__nameday_request.return_value = [
        {"date": "1212", "name": "Otília"},
    ]

    tzinfo = datetime.timezone(offset=datetime.timedelta(hours=1), name="Europe/Prague")
    timestamp = datetime.datetime(2022, 12, 12, 0, 1, 0, tzinfo=tzinfo)
    message = SlovakNamedayMessage(timestamp)

    message.nameday.request = mock.MagicMock()
    message.nameday.request.return_value = {}

    expected = (
        "Je pondělok 12. 12. 2022, 50. týždeň roka. Meniny má Otília."
        "\n\n#kalendár #meniny #slovak"
    )
    assert expected == str(message)


@mock.patch("toot_calendar.nameday.SlovakNameday.request")
def test_SlovakNamedayMessage__20221224(m__nameday_request):
    m__nameday_request.return_value = [
        {"date": "2412", "name": "Eva"},
        {"date": "2412", "name": "Adam"},
    ]

    tzinfo = datetime.timezone(offset=datetime.timedelta(hours=1), name="Europe/Prague")
    timestamp = datetime.datetime(2022, 12, 24, 0, 1, 0, tzinfo=tzinfo)
    message = SlovakNamedayMessage(timestamp)

    message.nameday.request = mock.MagicMock()
    message.nameday.request.return_value = {}

    expected = (
        "Je sobota 24. 12. 2022. Meniny má Eva a Adam.\n\n#kalendár #meniny #slovak"
    )
    assert expected == str(message)


@mock.patch("toot_calendar.nameday.SlovakNameday.request")
def test_SlovakNamedayMessage__20230102(m__nameday_request):
    m__nameday_request.return_value = [
        {"date": "0102", "name": "Karina"},
        {"date": "0102", "name": "Alexandra"},
    ]

    tzinfo = datetime.timezone(offset=datetime.timedelta(hours=1), name="Europe/Prague")
    timestamp = datetime.datetime(2023, 1, 2, 0, 1, 0, tzinfo=tzinfo)
    message = SlovakNamedayMessage(timestamp)

    message.nameday.request = mock.MagicMock()
    message.nameday.request.return_value = {}

    expected = (
        "Je pondělok 2. 1. 2023, 1. týždeň roka. Meniny má Karina a Alexandra."
        "\n\n#kalendár #meniny #slovak"
    )
    assert expected == str(message)


@mock.patch("toot_calendar.nameday.SlovakNameday.request")
def test_SlovakNamedayMessage__20230102(m__nameday_request):
    m__nameday_request.return_value = []

    tzinfo = datetime.timezone(offset=datetime.timedelta(hours=1), name="Europe/Prague")
    timestamp = datetime.datetime(2023, 1, 1, 0, 1, 0, tzinfo=tzinfo)
    message = SlovakNamedayMessage(timestamp)

    message.nameday.request = mock.MagicMock()
    message.nameday.request.return_value = {}

    expected = "Je neděľa 1. 1. 2023. \n\n#kalendár #meniny #slovak"
    assert expected == str(message)
